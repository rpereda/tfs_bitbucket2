# README #

Want to copy of Bitbucket repo to TFS?

Preliminary:

1. Have git installed
1. Add ssh keys to TFS repository. 

Follow these steps

1. Open a command prompt. Cd to something like c:\Users\peredara
1. git clone git@bitbucket.org:rpereda/tfs_bitbucket2.git
1. git remote add origin http://dc01tfspocdv01:8080/tfs/Test%20Collection/_git/BitbucketTest2
1. git push -u origin --all
1. Verify by looking here: http://dc01tfspocdv01:8080/tfs/Test%20Collection/_git/BitbucketTest2